import { FlashCardAppPage } from './app.po';

describe('flash-card-app App', () => {
  let page: FlashCardAppPage;

  beforeEach(() => {
    page = new FlashCardAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
