// Firebase Configuation parameters.  Nothing should be changed or the database will break

export const FIREBASE_CONFIG = {
    firebase: {
        apiKey: "AIzaSyBp5W5Q6lEG5g6RbplVTV-o06hVyTluy7E",
        authDomain: "flash-card-app-443c3.firebaseapp.com",
        databaseURL: "https://flash-card-app-443c3.firebaseio.com",
        projectId: "flash-card-app-443c3",
        storageBucket: "flash-card-app-443c3.appspot.com",
        messagingSenderId: "234348101662"
    }
}