import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../constants/constants';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Deck } from '../interfaces/deck';

@Injectable()
export class FirebaseConfigService {

  deckInfo: Array<string> = [];
  deckIDs: Array<string> = [];
  deckDescriptions: Array<string> = [];
  deckTopics: Array<string> = [];
  decks: Array<Deck> = [];

  constructor(private db: AngularFireDatabase) {
    // this.decksSubscription = db.list<any>('decks').valueChanges().subscribe(items => {
    //   this.items = items;
    // });
  }

  getDeckList() {
    return this.db.object<string>('decklist');
  };


}
