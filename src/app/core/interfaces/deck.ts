export interface Deck {
    id?: string;
    description?: string;
    topic?: string;
}