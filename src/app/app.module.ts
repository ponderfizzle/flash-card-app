// Module configurations
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, NgForm } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, Location, LocationStrategy} from '@angular/common';

// Firebase and AngularFire2 Configurations
import { AngularFireModule } from 'angularfire2';
import { FirebaseConfigService } from './core/services/firebase-config.service';
import { FIREBASE_CONFIG } from './core/constants/constants';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Component Configurations
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AboutComponent } from './about/about.component';
import { ManageDecksComponent } from './manage-decks/manage-decks.component';
import { CreateDeckComponent } from './create-deck/create-deck.component';
import { ViewerComponent } from './viewer/viewer.component';



const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'about', component: AboutComponent},
  {path:'manage-decks', component: ManageDecksComponent},
  {path:'create-deck', component: CreateDeckComponent},
  {path:'viewer', component: ViewerComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AboutComponent,
    ManageDecksComponent,
    CreateDeckComponent,
    ViewerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG.firebase),
    AngularFireDatabaseModule,
    BrowserAnimationsModule
  ],
  providers: [FirebaseConfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }
