import { Component, OnInit } from '@angular/core';
import { FirebaseConfigService } from '../core/services/firebase-config.service';
import { Deck } from '../core/interfaces/deck';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private firebaseConfigService: FirebaseConfigService) { }

  decks: Array<Deck> = [];
  deckIDs: Array<string> = [];

  ngOnInit() {
    this.getAllDecks();
  }

  getAllDecks() {
    this.firebaseConfigService.getDeckList().snapshotChanges().map(action => {
      const $key = action.payload.key;
      const data = { $key, ...action.payload.val() };
      return data;
    }).subscribe(items => {
      this.deckIDs = Object.keys(items);
      this.deckIDs.shift();  // removing the top level key as it is not used for anything
      for (let i = 0; i < this.deckIDs.length; i++) {
        this.decks[i] = { id: this.deckIDs[i], description: items[this.deckIDs[i]]['description'], topic: items[this.deckIDs[i]]['topic'] };
      }
    });
  }
}
