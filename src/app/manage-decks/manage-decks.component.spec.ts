import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDecksComponent } from './manage-decks.component';

describe('ManageDecksComponent', () => {
  let component: ManageDecksComponent;
  let fixture: ComponentFixture<ManageDecksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDecksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDecksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
